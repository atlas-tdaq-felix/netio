#include "zmq.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>


const char* HELP =
    R"FOO(Usage: netio_zeromq_pingpong client [options]
netio_zeromq_pingpong server [options]

Uses ZeroMQ to perform throughput measurements to compare to netio.

Options are:

  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to or listen on the specified port. Default: 12345.
  -h              Display this help message.
)FOO";



typedef std::chrono::high_resolution_clock myclock;


void
server(unsigned short port)
{
  //  Prepare our context and socket
  zmq::context_t context (1);
  zmq::socket_t socket (context, ZMQ_REP);
  std::stringstream s;
  s << "tcp://*:" << port;
  socket.bind (s.str().c_str());

  zmq::message_t reply (5);
  memcpy (reply.data (), "pong", 5);


  while (true) {
    zmq::message_t request;

    //  Wait for next request from client
    socket.recv (&request);
    std::cout << "> "  << (char*) request.data() << std::endl;

    //  Send reply back to client
    socket.send (reply);
  }
}


void
client(std::string hostname, unsigned short port)
{
  zmq::context_t context (1);
  zmq::socket_t socket (context, ZMQ_REQ);
  
  std::stringstream s;
  s << "tcp://" << hostname << ":" << port;
  socket.connect (s.str().c_str());

  zmq::message_t request (5);
  memcpy (request.data (), "ping", 5);

  while(true) {
    auto t0 = myclock::now();
    socket.send (request);

    //  Get the reply.
    zmq::message_t reply;
    socket.recv (&reply);
    auto t1 = myclock::now();
    std::cout << "> " << (char*) reply.data() << std::endl;
    double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
    std::cout << seconds << std::endl;
  }
}


int main (int argc, char *argv[])
{
    unsigned short port = 12345;
    std::string host = "127.0.0.1";

    char opt;
    while ((opt = getopt(argc, argv, "H:p:h")) != -1)
    {
        switch (opt)
        {
        case 'H':
            host = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'h':
        default:
            std::cerr << HELP << std::endl;
            return -1;
        }
    }


    if(strcmp(argv[optind], "server") == 0)
      {
	server(port);
      }
    else if(strcmp(argv[optind], "client") == 0)
      {
	client(host, port);
      }
    else
      {
	std::cerr << "Unknown command: " << argv[optind] << std::endl;
	std::cerr << HELP << std::endl;
	return -1;
      }
    
    return 0;
}
