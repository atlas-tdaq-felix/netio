#include <iostream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <string>
#include <algorithm>
#include <unistd.h>
#include <ctime>
#include <chrono>
#include <csignal>

#include "netio/netio.hpp"

#define APPNAME "netio_cat"

const char* HELP =
    "Usage: " APPNAME " send [options]\n"
    "       " APPNAME " recv [options]\n"
    "       " APPNAME " publish TAG [options]\n"
    "       " APPNAME " subscribe [options]\n"
    "Send or receive message using NetIO."
    R"FOO(

Send options:
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port. Default: 12345.

Receive options:
  -p PORT         Listen on the specified port. Default: 12345.
  -d DELIMITER    Use the string DELIMITER to seperate messages in the input or
                  output. Default: '\n' (newline).
  -f FORMAT       Format of the output written to screen. Available formats:
                    'human': Writes human-readable output (Default)
                    'plain': Only print message contents
                    'json':  Output is encoded in JSON
                    'chk': Interpret the contents of the message as a uint64_t
                           and checks that it increases by 1 with each packet
                           (originally meant to be used with "felix-dcs -g")
  -e ENCODING     Encoding for the data part of messages. Available encodings:
                    'ascii':   Interprets messages as ASCII strings (Default)
                    'escaped': Prints messages as sequence of bytes using
                               hexadecimal escape encodings (e.g. \\xAF)
                    'hex':     Print message as hexdump
                    'raw':     Unformatted hexadecimal data string
                    'void':    Do not print message
  -z              Use zero-copy mode.

Publish options:
  The same options as for "send" apply. In addition:
  -t TAG          Publish using the given tag, 0x for hex. This option may be used multiple
                  times to publish on multiple tags at the same time.

                  Note: Messages will be published unless at least on tag is
                        specified.

Subscribe options:
  The same options as for "recv" apply. In addition:
  -t TAG          Subscribe to the given tag, 0x for hex. This option may be used multiple
                  times to subscribe to multiple tags.

Generic options:
  -b BACKEND      Use the specified netio backend. Default: posix.
  -n PAGES        Number of netio pages per connection. Default: 128.
  -g PAGESIZE     Netio page size in bytes. Default: 1048576.
  -s [ht|ll]      Select high-throughput (ht) or low-latency (ll) communication.
                  Default: ht (high-throughput).
  -h              Display this help message.
)FOO";

enum socket_type
{
  SOCKET_HT,
  SOCKET_LL
};

struct {
  bool zerocopy = false;
} app_settings;

struct input_reader {
public:
  input_reader()
  {}

  void next_message(netio::message& msg)
  {
    std::string str;
    std::getline(std::cin, str);
    msg = netio::message(str);
  }

  bool data_available()
  {
    return std::cin.good();
  }
};

struct output_writer {
private:
  std::string delim;

  enum {
    HUMAN,
    PLAIN,
    JSON,
    SEQUENCE_CHECKER
  } format;

  enum {
    ASCII,
    ESCAPE,
    HEX,
    RAW,
    VOID
  } encoding;

  std::string encode_msg_data_escape(netio::message& msg)
  {
    auto data = std::string((const char*)msg.data_copy().data(), msg.size());
    std::stringstream output;

    for(unsigned i = 0; i < data.size(); i++)
    {
      output << "\\x" << std::hex << std::setfill('0') << std::setw(2) << ((unsigned int)data[i] & 0xFF) << std::dec;
    }
    return output.str();
  }


  std::string encode_msg_data_hex(netio::message& msg)
  {
    auto data = std::string((const char*)msg.data_copy().data(), msg.size());
    std::stringstream output;

    for(unsigned i = 0; i < data.size(); i++)
    {
      if(i%32 == 0)
      {
        if(i != 0)
          output << '\n';
        output << "0x" << std::hex << std::setfill('0') << std::setw(8) << i << ": ";
      }
      output << std::hex << std::setfill('0') << std::setw(2) << ((unsigned int)data[i] & 0xFF) << " " << std::dec;
    }
    return output.str();
  }

  std::string encode_msg_data_raw(netio::message& msg)
  {
    auto data = std::string((const char*)msg.data_copy().data(), msg.size());
    std::stringstream output;

    for(unsigned i = 0; i < data.size(); i++)
    {
      output << std::hex << std::setfill('0') << std::setw(2) << ((unsigned int)data[i] & 0xFF) << " " << std::dec;
    }
    return output.str();
  }

  std::string encode_msg_data(netio::message& msg)
  {
    switch(encoding)
    {
      case ASCII:
        return std::string((const char*)msg.data_copy().data(), msg.size());
      case ESCAPE:
        return encode_msg_data_escape(msg);
      case HEX:
        return encode_msg_data_hex(msg);
      case RAW:
        return encode_msg_data_raw(msg);
      case VOID:
        return std::string();
    }
    return ""; // not reachable
  }

  void print_message_human(netio::endpoint& ep, netio::message& msg)
  {
    std::cout << ">>> message from " << ep.address() << ":" << ep.port() << ", "
              << "size=" << msg.size() << std::endl
              <<  encode_msg_data(msg) << std::endl
              << delim << std::flush;
  }

  void print_message_plain(netio::endpoint& ep, netio::message& msg)
  {
    std::cout << encode_msg_data(msg) << delim << std::flush;
  }

  void print_message_json(netio::endpoint& ep, netio::message& msg)
  {
    std::cout << "{ "
              << "\"from\": \"" << ep.address() << ":" << ep.port() << "\", "
              << "\"size\": " << msg.size() << ", "
              << "\"content\": \"";
    std::string s = encode_msg_data(msg);
    for(unsigned i=0; i<s.size(); i++)
    {
      char c = s[i];
      if(isprint(c)) {
        std::cout << c;
      } else {
        std::cout << "\\x" << std::hex << std::setfill('0') << std::setw(2) << (int)c << std::dec;
      }
    }
    std::cout << "\" " << "}";
    std::cout << delim << std::flush;
  }

  void check_sequence(netio::endpoint& ep, netio::message& msg)
  {
    if (encoding != VOID) {
        std::cout << encode_msg_data(msg) << std::endl;
    }

    static uint64_t last(0);
    uint8_t msgdata[16];
    msg.serialize_to_usr_buffer(msgdata);
    uint64_t data(*(uint64_t*)(msgdata+8));

    if (data != (last + 1)) {
      auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
      char timestr[100];
      std::strftime(timestr, sizeof(timestr), "%Y-%m-%d %H:%M:%S", std::localtime(&timenow));
      std::cout << "[" << timestr << "] " << "non consecutive messages: last " << last << " current " << data << std::endl;
    }
    last = data;
  }

public:
  output_writer(std::string delim, std::string formatstr, std::string encodingstr)
  : delim(delim)
  {
    if(formatstr == "human")      format = HUMAN;
    else if(formatstr == "plain") format = PLAIN;
    else if(formatstr == "json")  format = JSON;
    else if(formatstr == "chk")  format = SEQUENCE_CHECKER;
    else throw std::runtime_error("the specified output format is not valid");

    if(encodingstr == "ascii")         encoding = ASCII;
    else if(encodingstr == "escaped")  encoding = ESCAPE;
    else if(encodingstr == "hex")      encoding = HEX;
    else if(encodingstr == "raw")      encoding = RAW;
    else if(encodingstr == "void")     encoding = VOID;
    else throw std::runtime_error("the specified output encoding is not valid");
  }

  void print_message(netio::endpoint& ep, netio::message& msg)
  {
    switch(format)
    {
      case HUMAN:
        print_message_human(ep, msg);
        break;
      case PLAIN:
        print_message_plain(ep, msg);
        break;
      case JSON:
        print_message_json(ep, msg);
        break;
      case SEQUENCE_CHECKER:
        check_sequence(ep, msg);
        break;
    }
  }
};


struct configuration{
  std::string task;
  unsigned short port;
  std::string host;
  std::string backend;
  std::string format;
  std::string encoding;
  std::string delimiter;
  socket_type sockettype;
  std::vector<netio::tag> tags;
  netio::context* ctx;
  netio::low_latency_subscribe_socket* ll_sub_socket;
  netio::subscribe_socket* sub_socket;
} cfg;



static void
send_ll(input_reader& in)
{
  netio::sockcfg config = netio::sockcfg::cfgll();

  netio::low_latency_send_socket socket(cfg.ctx, config);
	socket.connect(netio::endpoint(cfg.host.c_str(), cfg.port));

  netio::message msg;
  while(in.data_available()) {
    in.next_message(msg);
    socket.send(msg);
  }
}

static void
send_ht(input_reader& in)
{
  netio::sockcfg config = netio::sockcfg::cfg();

  netio::buffered_send_socket socket(cfg.ctx, config);
	socket.connect(netio::endpoint(cfg.host.c_str(), cfg.port));

  netio::message msg;
  while(in.data_available()) {
    in.next_message(msg);
    socket.send(msg);
  }
  socket.flush();
}

static void
recv_ll(output_writer& out)
{
  netio::sockcfg config = netio::sockcfg::cfgll();
  if(app_settings.zerocopy)
    config(netio::sockcfg::ZERO_COPY);

  netio::low_latency_recv_socket socket(cfg.ctx, cfg.port, [&](netio::endpoint& ep, netio::message& msg){
    out.print_message(ep, msg);
  }, config);
  while(true) {
    usleep(100*1000);
  }
}

static void
recv_ht(output_writer& out)
{
  netio::sockcfg config = netio::sockcfg::cfg();
  if(app_settings.zerocopy)
    config(netio::sockcfg::ZERO_COPY);

  netio::recv_socket socket(cfg.ctx, cfg.port, config);
  netio::endpoint ep;
  netio::message msg;
  while(true) {
    socket.recv(ep, msg);
    out.print_message(ep, msg);
  }
}

static void
publish(input_reader& in)
{
	netio::publish_socket socket(cfg.ctx, cfg.port);

  netio::message msg;
  while(in.data_available())
  {
    in.next_message(msg);
    for(unsigned i=0; i<cfg.tags.size(); i++)
    {
      socket.publish(cfg.tags[i], msg);
    }
  }
}


static void
subscribe_ll(output_writer& out)
{
  netio::sockcfg config = netio::sockcfg::cfgll();
  if(app_settings.zerocopy)
    config(netio::sockcfg::ZERO_COPY);

	cfg.ll_sub_socket = new netio::low_latency_subscribe_socket(cfg.ctx,
    [&](netio::endpoint& ep, netio::message& msg)
  {
    out.print_message(ep, msg);
  }, config);

  for(unsigned i=0; i<cfg.tags.size(); i++)
  {
    cfg.ll_sub_socket->subscribe(cfg.tags[i], netio::endpoint(cfg.host, cfg.port));
  }

  while(true) {
    usleep(100*1000);
  }
}


static void
subscribe_ht(output_writer& out)
{
  netio::sockcfg config = netio::sockcfg::cfg();
  if(app_settings.zerocopy)
    config(netio::sockcfg::ZERO_COPY);

	cfg.sub_socket = new netio::subscribe_socket(cfg.ctx, config);
  for(unsigned i=0; i<cfg.tags.size(); i++)
  {
    cfg.sub_socket->subscribe(cfg.tags[i], netio::endpoint(cfg.host, cfg.port));
  }

  netio::endpoint ep;
  netio::message msg;

  while(true) {
    cfg.sub_socket->recv(ep, msg);
    out.print_message(ep, msg);
  }
}

static void
unsubscribe()
{
  std::cout << "Unsubscribing..." << std::endl;
  if (cfg.sockettype == SOCKET_HT){
    for(auto t : cfg.tags){cfg.sub_socket->unsubscribe(t, netio::endpoint(cfg.host, cfg.port));}
  }
  else if (cfg.sockettype == SOCKET_LL){for(auto t : cfg.tags){cfg.ll_sub_socket->unsubscribe(t, netio::endpoint(cfg.host, cfg.port));}
  }
  else throw std::runtime_error("the specified output format is not valid");
  return;
}


std::thread bg_thread;

void signalHandler( int signum ) {
  std::cout << "Interrupt signal (" << signum << ") received." << std::endl; fflush(stdout);
  if (cfg.task == "subscribe") {
    unsubscribe();
  }
  cfg.ctx->event_loop()->stop();
  bg_thread.join();
  exit(signum);
}


int
main(int argc, char** argv)
{
  signal(SIGINT, signalHandler);

	cfg.port = 12345;
	cfg.host = "127.0.0.1";
	cfg.backend = "posix";
  cfg.format = "human";
  cfg.encoding = "ascii";
  cfg.delimiter = "\n";
  cfg.sockettype = SOCKET_HT;
  cfg.task = std::string(argv[optind]);

	char opt;
  std::string temp;
	while ((opt = getopt(argc, argv, "b:H:p:f:e:n:g:d:s:t:zh")) != -1)
	{
		switch (opt)
		{
		case 'b':
			cfg.backend = optarg;
			break;
		case 'H':
			cfg.host = optarg;
			break;
		case 'p':
			cfg.port = atoi(optarg);
			break;
    case 'f':
      cfg.format = optarg;
      std::transform(cfg.format.begin(), cfg.format.end(), cfg.format.begin(), ::tolower);
      break;
    case 'e':
      cfg.encoding = optarg;
      std::transform(cfg.encoding.begin(), cfg.encoding.end(), cfg.encoding.begin(), ::tolower);
      break;
    case 'd':
      cfg.delimiter = optarg;
      break;
    case 'z':
      app_settings.zerocopy = true;
      break;
    case 't':
      cfg.tags.push_back(std::stol(optarg, nullptr, 0));
      break;
    case 's':
      temp = optarg;
      std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
      if(temp == "ll") {
        cfg.sockettype = SOCKET_LL;
      } else if(temp == "ht") {
        cfg.sockettype = SOCKET_HT;
      } else {
        std::cerr << APPNAME << ": error: socket type must be either 'HT' or 'LL'";
        return -1;
      }
      break;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

	if(optind >= argc)
	{
		std::cerr << APPNAME << ": error: too many arguments\n\n" << HELP << std::endl;
		return -1;
	}

	cfg.ctx = new netio::context(cfg.backend.c_str());

  netio::context* ctx_ptr = cfg.ctx;
	bg_thread = std::thread([ctx_ptr](){ctx_ptr->event_loop()->run_forever();});

	if(strcmp(argv[optind], "send") == 0)
	{
    input_reader in;
    if (cfg.sockettype == SOCKET_LL) {
      send_ll(in);
    } else if(cfg.sockettype == SOCKET_HT) {
      send_ht(in);
    }
	}
  else if(strcmp(argv[optind], "recv") == 0)
	{
    output_writer out(cfg.delimiter, cfg.format, cfg.encoding);
    if (cfg.sockettype == SOCKET_LL) {
      recv_ll(out);
    } else if(cfg.sockettype == SOCKET_HT) {
      recv_ht(out);
    }
	}
  else if(strcmp(argv[optind], "publish") == 0)
	{
    input_reader in;
    publish(in);
	}
  else if(strcmp(argv[optind], "subscribe") == 0)
	{
    output_writer out(cfg.delimiter, cfg.format, cfg.encoding);
    if (cfg.sockettype == SOCKET_LL) {
      subscribe_ll(out);
    } else if(cfg.sockettype == SOCKET_HT) {
      subscribe_ht(out);
    }
	}
	else
	{
		std::cerr << APPNAME << ": error: Unknown command: " << argv[optind] << std::endl << std::endl;
		std::cerr << HELP << std::endl;
		return -1;
	}
	return 0;
}
