#include <iostream>
#include <chrono>
#include <string>
#include <unistd.h>

#include "netio/netio.hpp"

const char* HELP =
    R"FOO(Usage: netio_recv [options]

Listens on a port and receives incoming netio messages.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -p PORT         Listen on the specified port on the remote. Default: 12345.
  -B PAGESIZE     Pagesize in byte. Default: 1 MB.
  -z              Enable zero-copy mode.
  -h              Display this help message.
)FOO";


int
main(int argc, char** argv)
{
	unsigned short port = 12345;
	std::string backend = "posix";
	size_t pagesize = 1024*1024;
  bool zerocopy = false;

	char opt;
	while ((opt = getopt(argc, argv, "b:B:p:zh")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'B':
		        pagesize = std::stoull(optarg);
                        break;
		case 'p':
			port = atoi(optarg);
			break;
    case 'z':
      zerocopy = true;
      break;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}


	netio::context ctx(backend.c_str());
  netio::sockcfg cfg = netio::sockcfg::cfg();
  cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, 16);
  cfg(netio::sockcfg::PAGESIZE, pagesize);
  if(zerocopy)
    cfg(netio::sockcfg::ZERO_COPY);
	netio::recv_socket socket(&ctx, port, cfg);


	std::atomic<unsigned long long> messages_received;
	std::atomic<unsigned long long> bytes_received;
	std::atomic_bool running;
	running.store(true);

	typedef std::chrono::high_resolution_clock clock;
	auto t0 = clock::now();

	std::thread statistics_thread([&](){
	    while(running.load())
	      {
		auto t1 = clock::now();
		double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
		unsigned long long messages_delta = std::atomic_exchange(&messages_received, 0ull);
		unsigned long long bytes_delta = std::atomic_exchange(&bytes_received, 0ull);

		std::cout << "Statistics - timestamp " << std::chrono::duration_cast<std::chrono::seconds>(t1.time_since_epoch()).count() << std::endl;
		std::cout << "  timedelta [s]: " << seconds << std::endl;
		std::cout << "  data received [MB]: " << bytes_delta/1024./1024. << std::endl;
		std::cout << "  messages_received: " << messages_delta << std::endl;
		std::cout << "  message rate [kHz]: " << messages_delta/seconds/1000 << std::endl;
		std::cout << "  data rate [GB/s]: " << bytes_delta/seconds/1024./1024./1024. << std::endl;
		std::cout << "  data rate [Gb/s]: " << bytes_delta/seconds/1024./1024./1024.*8 << std::endl;
		if(messages_delta > 0)
		  std::cout << "  average message size [Byte]: " << bytes_delta/messages_delta << std::endl;
		t0 = t1;

		sleep(1);
	      }
	  });



	std::thread bg_thread([&ctx](){
	   ctx.event_loop()->run_forever();
	});

	netio::message msg;
	while(true)
	{
	        socket.recv(msg);
		messages_received ++;
		bytes_received += msg.size();
	}

	ctx.event_loop()->stop();
	running.store(false);
	bg_thread.join();
	statistics_thread.join();
	return 0;
}
