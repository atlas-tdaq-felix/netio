#include <iostream>
#include <string>
#include <chrono>
#include <unistd.h>

#include "netio/netio.hpp"


const char* HELP =
    R"FOO(Usage: netio_bw [options]

Throughput benchmark measurements with NetIO.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -c HOSTNAME     Connect as client to the specified hostname.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -s              Run in server mode.
  -n MESSAGES     The total amount of messages that is transferred.
                  Default: 1000.
  -z              Enable zerocopy mode.
  -h              Display this help message.
)FOO";

typedef std::chrono::high_resolution_clock stdclock;

struct bm_settings
{
  char ip[16];
  unsigned short port;
  unsigned n_messages;
  size_t msgsize;
};



static bool zerocopy = false;
static netio::context* pctx;

double start_client(std::string host, unsigned n_messages, size_t msgsize, unsigned short port)
{

  netio::sockcfg cfg = netio::sockcfg::cfg();
  if(zerocopy)
    cfg(netio::sockcfg::ZERO_COPY);
  netio::recv_socket rs(pctx, 0, cfg);

  bm_settings bm;
  bm.msgsize = msgsize;
  bm.n_messages = n_messages;
  bm.port = rs.listen_endpoint().port();
  sprintf(bm.ip, "192.168.100.4");
  sleep(1);

  netio::low_latency_send_socket lls(pctx);
  lls.connect(netio::endpoint(host, port));
  while(!lls.is_open())
      pctx->event_loop()->run_for(1000);
  netio::message m((uint8_t*)(&bm), sizeof(bm_settings));
  lls.send(m);

  sleep(1);

  netio::message msg;
  std::chrono::time_point<stdclock> t0, t1;
  for(unsigned i=0; i<n_messages; i++)
  {
    rs.recv(msg);
    if(i==0)
    {
      t0 = stdclock::now();
    }
    if(i==n_messages-1)
    {
      t1 = stdclock::now();
    }
  }

  double seconds = std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
  return seconds;
}


void start_server(netio::endpoint& ep, netio::message& m)
{
  bm_settings* bm = (bm_settings*)m.fragment_list()->data[0];

  char* data = new char[bm->msgsize];
  for(unsigned int i=0; i<bm->msgsize; i++)
  {
    data[i] = 'x';
  }
  netio::message msg((uint8_t*)data, bm->msgsize);

  netio::sockcfg cfg = netio::sockcfg::cfg();
  if(zerocopy)
    cfg(netio::sockcfg::ZERO_COPY);

  netio::buffered_send_socket send_socket(pctx, cfg);
  send_socket.connect(netio::endpoint(bm->ip, bm->port));
  while(!send_socket.is_open())
      pctx->event_loop()->run_for(1000);

  for(unsigned i=0; i<bm->n_messages+1; i++)
  {
    send_socket.send(msg);
  }
  send_socket.flush();
  delete[] data;
}

int main(int argc, char** argv)
{
  unsigned short port = 12345;
  unsigned n_messages = 1000;
  std::string host = "127.0.0.1";
  std::string backend = "posix";
  bool client = false;
  bool server = false;

  char opt;
  while ((opt = getopt(argc, argv, "b:c:p:b:n:szh")) != -1)
  {
    switch (opt)
    {
    case 'b':
      backend = optarg;
      break;
    case 'c':
      client = true;
      host = optarg;
      break;
    case 'p':
      port = atoi(optarg);
      break;
    case 'n':
      n_messages = atoi(optarg);
      break;
    case 's':
      server = true;
      break;
    case 'z':
      zerocopy = true;
      break;
    case 'h':
    default:
      std::cerr << HELP << std::endl;
      return -1;
    }
  }

  if(!(client || server))
  {
    std::cerr << "Either -c or -s must be given" << std::endl;
    return -1;
  }
  if(client && server)
  {
    std::cerr << "The options -c or -s can not be used together" << std::endl;
    return -1;
  }

  netio::context ctx(backend.c_str());
  pctx = &ctx;

  std::thread bg_thread([&ctx](){
    ctx.event_loop()->run_forever();
  });


  if(server)
  {
    netio::low_latency_recv_socket recv_socket(&ctx, port, start_server);
    while(true) sleep(1);
  }
  if(client)
  {
    for(size_t msgsize = 16; msgsize <= 1048576; msgsize *= 2)
    {
      double seconds = start_client(host, n_messages, msgsize, port);
      double Gbps = (double)n_messages*msgsize*8/1000/1000/1000/seconds;

      std::cout << msgsize << "\t  " << seconds << " \t " << Gbps << std::endl;
    }
  }


  ctx.event_loop()->stop();
	bg_thread.join();
}
