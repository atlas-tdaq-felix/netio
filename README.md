# NetIO

[![build status](https://gitlab.cern.ch/atlas-tdaq-felix/netio/badges/master/build.svg)](https://gitlab.cern.ch/atlas-tdaq-felix/netio/commits/master)


NetIO is an asynchronous, message-based network communication library. 
NetIO supports different backends. Currently Ethernet and Infiniband are
supported, OmniPath support is experimental.

NetIO supports the following communication patterns:
* Low-latency point-to-point
* High-throughput point-to-point
* Low-latency publish/subscribe
* High-throughput publish/subscribe


## Authors

* Jörn Schumacher <joern.schumacher@cern.ch> (main author, contact for help)
* Daniel Guest
* Tomasz Sodzawiczny 


## Compilation

At the moment NetIO has to be compiled as part of a FELIX release.


## Creating binary and source distribution

For binary distributions, run
    
    make package

For source distributions, run

	make dist


## Tests

The unit test suite can be run with

    ./netio_test


## Examples

See the following files for example programs:

* src/main_netio_pubsub.cpp
* src/main_netio_throughput.cpp
* src/main_netio_recv.cpp
