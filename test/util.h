#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>

inline
int get_free_port() {

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        printf("socket error\n");
        return -1;
    }
    // printf("Opened %d\n", sock);

    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = 0;
    if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        if(errno == EADDRINUSE) {
            printf("the port is not available. already to other process\n");
            return -1;
    } else {
            printf("could not bind to process (%d) %s\n", errno, strerror(errno));
            return -1;
        }
    }

    socklen_t len = sizeof(serv_addr);
    if (getsockname(sock, (struct sockaddr *)&serv_addr, &len) == -1) {
        perror("getsockname");
        return -1;
    }

    int port = ntohs(serv_addr.sin_port);

    // printf("port number %d\n", port);

    if (close(sock) < 0 ) {
        printf("did not close: %s\n", strerror(errno));
        return -1;
    }

    return port;
}


inline const char*
test_iface_addr()
{
    const char* addr = std::getenv("NETIO_TEST_IFACE");
    if (addr) return addr;

    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    static char ip[NI_MAXHOST];

    // return cached value
    if (strcmp(ip, "")) {
        return ip;
    }

    if (getifaddrs(&ifaddr) == -1) {
        printf("getifaddrs problem\n");
        exit(EXIT_FAILURE);
    }

    char ifaces[] = "eth-100-0, eth-25-0, eth0";
    char seps[] = ", ";
    char* iface;

    iface = strtok(ifaces, seps);
    while (iface != NULL) {
        // printf("Looking for %s\n", iface);
        // Look through all existing interfaces
        for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
            if (ifa->ifa_addr == NULL)
                continue;

            s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if (s != 0)
                continue;

            // printf("Trying %s\n", ifa->ifa_name);
            if (((strcmp(ip, iface) == 0) || (strcmp(ifa->ifa_name, iface) == 0)) && (ifa->ifa_addr->sa_family==AF_INET)) {
                printf("Interface : <%s>\n", ifa->ifa_name);
                printf("Address : <%s>\n", ip);
                freeifaddrs(ifaddr);
                return ip;
            }
        }

        // next on the list
        iface = strtok(NULL, seps);
    }

    // not found
    printf("NETIO_TEST_IFACE is not set, using 127.0.0.1 as loopback address.\n");
    printf("If fi_endpoint fails, consider setting NETIO_TEST_IFACE to the RDMA device's IP address.\n");
    addr = "127.0.0.1";
    freeifaddrs(ifaddr);
    return addr;
}
