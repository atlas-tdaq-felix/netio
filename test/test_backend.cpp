#include "catch/catch.hpp"

#include "netio/netio.hpp"
#include "../src/posix.hpp"
#include "util.h"

using namespace netio;

TEST_CASE( "create a listen socket", "[posix]" )
{
    int port = get_free_port();

    event_loop evloop;
    netio::context ctx("posix");
    posix_listen_socket socket(&evloop, endpoint(test_iface_addr(), port), &ctx);
    socket.listen();
    evloop.run_for(1000);

    REQUIRE( true );
}

TEST_CASE( "connect to a listen socket", "[posix]" )
{
    int port = get_free_port();

    event_loop evloop;

    netio::context ctx("posix");
    posix_listen_socket listen_socket(&evloop, endpoint(test_iface_addr(), port), &ctx);
    posix_send_socket send_socket(&evloop);

    listen_socket.listen();
    send_socket.connect(endpoint(test_iface_addr(), port));
    evloop.run_for(1000);

    REQUIRE( true );
}


TEST_CASE( "connect and send a buffer", "[posix]" )
{
    int port = get_free_port();

    netio::context ctx("posix");
    event_loop evloop;
    posix_listen_socket listen_socket(&evloop, endpoint(test_iface_addr(), port), &ctx);
    posix_send_socket send_socket(&evloop);

    //unsigned msgs_received = 0;

    listen_socket.listen();
    send_socket.connect(endpoint(test_iface_addr(), port));


    netio::reusable_buffer* b = new netio::reusable_buffer(12+sizeof(netio::msgheader), NULL, &ctx);
    netio::msgheader header;
    header.len = 12;
    b->buffer()->append((char*)&header, sizeof(netio::msgheader));
    b->buffer()->append("hello world!", 12);
    send_socket.send_buffer(b);
    evloop.run_for(1000);

    listen_socket.process_page();
    netio::message msg;
    listen_socket.pop_message_entry(&msg);

    REQUIRE( msg.size() == 12 );
    REQUIRE( strncmp((char*)msg.fragment_list()->data[0], "hello world!", 12) == 0 );
}
