#include "catch/catch.hpp"

#include "netio/netio.hpp"
#include "util.h"

using namespace netio;

TEST_CASE( "create a low-latency send socket", "[sockets]" )
{
    context ctx("posix");
    low_latency_send_socket socket(&ctx);

    REQUIRE( true );
}


TEST_CASE( "send and receive from a low-latency socket", "[sockets]")
{
    int port = get_free_port();

    context ctx("posix");
    low_latency_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    send.connect(endpoint(test_iface_addr(), port));
    send.send(message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}


TEST_CASE( "send and receive a large message from a low-latency socket", "[sockets]")
{
    int port = get_free_port();

    context ctx("posix");
    low_latency_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    const size_t SIZE = 40000;
    char* d = new char[SIZE];
    for(unsigned i=0; i<SIZE; i++)
    {
        d[i] = (2*i + 13) % 23;
    }

    send.connect(endpoint(test_iface_addr(), port));
    send.send(message((uint8_t*)d, SIZE));
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == SIZE );
    for(unsigned i=0; i<SIZE; i++)
    {
        REQUIRE( d[i] == received[i] );
    }
}


TEST_CASE( "close a recv socket", "[sockets]")
{
    int port = get_free_port();

    context ctx("posix");
    recv_socket recv(&ctx, port);

    recv.close();

    message received;
    REQUIRE_THROWS_AS( recv.recv(received), netio::connection_closed );
}


TEST_CASE( "create a buffered send socket", "[sockets]" )
{
    context ctx("posix");
    buffered_send_socket socket(&ctx);

    REQUIRE( true );
}


TEST_CASE( "send and receive from a buffered socket", "[sockets]")
{
    int port = get_free_port();

    context ctx("posix");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    send.connect(endpoint(test_iface_addr(), port));
    send.send(message((uint8_t*)"hello, world!", 14));
    send.flush();
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}


TEST_CASE( "send and receive from a buffered socket without a flush", "[sockets]")
{
    int port = get_free_port();

    context ctx("posix");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    send.connect(endpoint(test_iface_addr(), port));
    send.send(message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(4000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}


TEST_CASE( "send and receive large message from a buffered socket", "[sockets]" )
{
    int port = get_free_port();

    context ctx("posix");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port);

    const size_t SIZE = 40000;
    char* d = new char[SIZE];
    REQUIRE( d != NULL );
    message m((uint8_t*)d, SIZE);
    CAPTURE(m.size());

    send.connect(endpoint(test_iface_addr(), port));
    send.send(m);
    send.flush();
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == SIZE );
    delete[] d;
}


TEST_CASE( "connect to a publish socket", "[sockets]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port);
    subscribe_socket sub(&ctx);

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    pub.register_unsubscribe_callback([&](tag, endpoint)
    {
        subscribed = false;
    });

    sub.subscribe(0, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == true );

    sub.unsubscribe(0, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == false );
}


TEST_CASE( "connect to a publish socket using low-latency subscribe", "[sockets]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port);
    low_latency_subscribe_socket sub(&ctx, [](endpoint&, message&) {});

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    pub.register_unsubscribe_callback([&](tag, endpoint)
    {
        subscribed = false;
    });

    sub.subscribe(0, endpoint(test_iface_addr(), port));

    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == true );

    sub.unsubscribe(0, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == false );
}



TEST_CASE( "publish with a publish socket", "[sockets]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port);
    subscribe_socket sub(&ctx);

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(5, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(500);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(5000);

    message received;
    sub.recv(received);

    REQUIRE( subscribed == true );
    REQUIRE( received.size() == 14 );
}

TEST_CASE( "publish with a publish socket - low-latency", "[sockets]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port);

    bool message_received = false;
    low_latency_subscribe_socket sub(&ctx,
                                     [&message_received](endpoint&, message&)
    {
        message_received = true;
    });

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(5, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(500);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(5000);

    REQUIRE( subscribed == true );
    REQUIRE( message_received == true );

    message_received = false;
    sub.unsubscribe(5, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(500);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(5000);
    REQUIRE( message_received == false );
}

bool message_received = false;
void cb(uint8_t*, size_t)
{
    message_received = true;
}

TEST_CASE( "publish with a publish socket and receive with callback", "[sockets]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port);

    subscribe_socket sub(&ctx, sockcfg::cfg()(sockcfg::CALLBACK, (uint64_t)cb));

    sub.subscribe(5, endpoint(test_iface_addr(), port));
    ctx.event_loop()->run_for(500);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(5000);

    REQUIRE( message_received == true );
}

TEST_CASE( "subscribe to many tags", "[sockets]" )
{
    const unsigned N = 100; // used to be 1000

    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port);
    subscribe_socket sub(&ctx);

    unsigned subscribed = 0;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed++;
    });

    unsigned unsubscribed = 0;
    pub.register_unsubscribe_callback([&](tag, endpoint)
    {
        unsubscribed++;
    });

    for(unsigned i=0; i<N; i++)
    {
      sub.subscribe(i, endpoint(test_iface_addr(), port));
    }
    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == N );

    for(unsigned i=0; i<N; i++)
    {
      sub.unsubscribe(i, endpoint(test_iface_addr(), port));
    }
    ctx.event_loop()->run_for(1000);
    REQUIRE( unsubscribed == N );
}
